package com.example.todo.modal;

import android.os.Parcel;
import android.os.Parcelable;

public class Task extends Object implements Parcelable {
    private int id;
    private String taskName;
    private long date;
    private boolean priority;

    public Task() {
    }

    public Task(Parcel in) {
        id = in.readInt();
        taskName = in.readString();
        date = in.readLong();
        priority = in.readInt() == 1;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isPriority() {
        return priority;
    }

    public void setPriority(boolean priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id" + id + '\'' +
                "taskName='" + taskName + '\'' +
                ", date=" + date +
                ", priority=" + priority +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.taskName);
        dest.writeLong(this.date);
        dest.writeInt(this.priority ? 1 : 0);
    }
}
