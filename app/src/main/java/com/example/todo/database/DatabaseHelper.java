package com.example.todo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "ToDoDB";
    private Context context;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE IF NOT EXISTS ToDoTable(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, task_name VARCHAR, due_date NUMERIC, priority BLOB);");
        } catch (SQLiteException e) {
            try {
                throw new IOException(e);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+"ToDoTable");
        onCreate(db);
    }

    public boolean insert(String taskName, Calendar dueDate, Boolean priority) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("task_name", taskName);
        contentValues.put("due_date", dueDate.getTimeInMillis());
        contentValues.put("priority", priority);
        db.replace("ToDoTable", null, contentValues);

        Toast.makeText(context, "Data Inserted Successfully", Toast.LENGTH_LONG).show();
        return true;
    }

    public boolean update(String taskName, Calendar dueDate, int taskId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("task_name", taskName);
        contentValues.put("due_date", dueDate.getTimeInMillis());
        db.update("ToDoTable",contentValues,"id="+taskId,null);
        return true;
    }

    public boolean delete(int taskId){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("ToDoTable","id="+taskId,null);
        return true;
    }

    public Cursor fetchAllTask() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + "ToDoTable", null);
        return cursor;
    }
}
