package com.example.todo.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.widget.Toast;

public class TaskReminderService extends JobService {

    @Override
    public boolean onStartJob(JobParameters job) {
        System.out.println("===============onStartJob===============");
        Toast.makeText(getApplicationContext(), "Service is Started", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }
}
