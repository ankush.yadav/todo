package com.example.todo.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import com.example.todo.MainActivity;
import com.example.todo.R;
import com.example.todo.modal.Task;

public class NotificationReceiver extends BroadcastReceiver {
    public static final String NOTIFICATION = "notification";
    public static final String NOTIFICATION_ID = "notification_id";
    public static final String CHANNEL_ID = "com.example.todo.NotificationPath";
    private NotificationCompat.Builder mNotificationCompactBuilder;

    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("===============Start onReceive===============");
        Intent notificationIntent = new Intent(context, MainActivity.class);
        String taskName = intent.getStringExtra(NOTIFICATION);
        int alarmId = intent.getIntExtra(NOTIFICATION_ID,0) ;
        System.out.println("alarmId --> "+alarmId);
        System.out.println("task --> "+taskName);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, alarmId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "My Example", NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(channel);
        }
        mNotificationCompactBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentIntent(pendingIntent)
                .setContentTitle("Pending Task")
                .setContentText("last half an hour is remaining to complete the your "  +taskName+ " task")
                .setAutoCancel(true);
        manager.notify(MainActivity.JOB_ID, mNotificationCompactBuilder.build());
    }
}
