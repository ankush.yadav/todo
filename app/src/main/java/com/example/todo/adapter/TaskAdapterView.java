package com.example.todo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.example.todo.R;
import com.example.todo.database.DatabaseHelper;
import com.example.todo.modal.Task;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class TaskAdapterView extends RecyclerView.Adapter<TaskAdapterView.TaskViewHolder> implements PopupMenu.OnMenuItemClickListener {

    private List<Task> mTaskList;
    private OnTaskClickListener listener;
    private Task task;

    public TaskAdapterView(List<Task> taskList, OnTaskClickListener listener) {
        mTaskList = taskList;
        this.listener =  listener;
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TaskViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.task_row, parent, false));
    }

    @Override
    public int getItemCount() {
        return mTaskList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        task = mTaskList.get(position);
        holder.tvTaskName.setText(task.getTaskName());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(task.getDate());
        holder.tvDueDate.setText(String.format(Locale.US,"%d/%d/%d", calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR)));
        holder.ivTaskOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu((Context) listener, v);
                popup.setOnMenuItemClickListener(TaskAdapterView.this);
                popup.inflate(R.menu.task_menu_option);
                popup.show();
            }
        });
        holder.taskView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTaskClick(task);
            }
        });
    }

    class TaskViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTaskName;

        private TextView tvDueDate;
        private ImageView ivTaskStatus;
        private ImageView ivTaskOption;
        private View taskView;

        public TaskViewHolder(@NonNull View taskView) {
            super(taskView);
            tvTaskName = taskView.findViewById(R.id.tv_task_name);
            tvDueDate = taskView.findViewById(R.id.tv_due_date);
            ivTaskStatus = taskView.findViewById(R.id.iv_task_icon);
            ivTaskOption = taskView.findViewById(R.id.iv_task_option);
            this.taskView = taskView;
        }

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.update_task:
                listener.onTaskClick(task);
                break;
            case R.id.delete_task:
                DatabaseHelper databaseHelper = new DatabaseHelper((Context) listener);
                databaseHelper.delete(task.getId());
                break;
            default:
        }
        return false;
    }

    public interface OnTaskClickListener {
        void onTaskClick(Task task);
    }
}