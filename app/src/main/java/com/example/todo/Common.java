package com.example.todo;

import com.example.todo.modal.Country;
import com.example.todo.modal.State;

import java.util.ArrayList;
import java.util.List;

public class Common {
    public static final String EXTRA_TASK_NAME = "TaskName";
    public static final String EXTRA_TASK_ID = "TaskId";
    public static final String EXTRA_TASK = "Task";

    private static List<State> dummyStateList = new ArrayList<>();
    private static List<Country> dummyCountryList = new ArrayList<>();
    private static List<State> states = new ArrayList<>();

    private static void dummyStates() {
        dummyStateList.add(new State(0, 0, "Maharashtra"));
        dummyStateList.add(new State(1, 0, "Rajasthan"));
        dummyStateList.add(new State(2, 0, "Gujurat"));
        dummyStateList.add(new State(3, 0, "Uttar Pradesh"));
        dummyStateList.add(new State(4, 0, "Goa"));


        dummyStateList.add(new State(5, 1, "California"));
        dummyStateList.add(new State(6, 1, "Texas"));
        dummyStateList.add(new State(7, 1, "Hawaii"));
        dummyStateList.add(new State(8, 1, "New Jersey"));
        dummyStateList.add(new State(9, 1, "Arizona"));

        dummyStateList.add(new State(10, 2, "Hubei"));
        dummyStateList.add(new State(11, 2, "Sichuan"));
        dummyStateList.add(new State(12, 2, "Hunan"));
        dummyStateList.add(new State(13, 2, "Jiangsu"));
        dummyStateList.add(new State(14, 2, "Shandong"));

        dummyStateList.add(new State(15, 3, "Hokkaidō"));
        dummyStateList.add(new State(16, 3, "Aomori"));
        dummyStateList.add(new State(17, 3, "Iwate"));
        dummyStateList.add(new State(18, 3, "Niigata"));
        dummyStateList.add(new State(19, 3, "Tottori"));

        dummyStateList.add(new State(20, 4, "New South Wales"));
        dummyStateList.add(new State(20, 4, "Victoria"));
        dummyStateList.add(new State(20, 4, "Western Australia"));
        dummyStateList.add(new State(20, 4, "South Australia"));
        dummyStateList.add(new State(20, 4, "Tasmania"));
    }

    private static void dummyCountryList() {
        dummyCountryList.add(new Country(0, "India"));
        dummyCountryList.add(new Country(1, "US"));
        dummyCountryList.add(new Country(2, "China"));
        dummyCountryList.add(new Country(3, "Japan"));
        dummyCountryList.add(new Country(4, "Australia"));
    }

    static List<Country> getCountry() {
        dummyStates();
        dummyCountryList();
        return dummyCountryList;
    }

    static List<State> getStateByCountry(int countryId) {
        states.clear();
        for (State state : dummyStateList) {
            if (state.getCountryId() == countryId)
                states.add(state);
        }
        return states;
    }
}
