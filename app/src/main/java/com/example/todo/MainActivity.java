package com.example.todo;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.todo.adapter.TaskAdapterView;
import com.example.todo.database.DatabaseHelper;
import com.example.todo.modal.Task;
import com.example.todo.service.NotificationReceiver;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements TaskAdapterView.OnTaskClickListener {

    private List<Task> mTaskList;
    public static final int JOB_ID = 99;

    private DatabaseHelper databaseHelper;

    private RecyclerView rvTask;
    private TaskAdapterView taskAdapterView;
    private LinearLayout addView;
    private AlarmManager alarmManager;
    private Button applyButton;
    private Button btMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getView();
        init();
        setListener();
        operation();
    }

    private void operation() {
        rvTask.setAdapter(taskAdapterView);
    }

    private void setListener() {
        addView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddTaskActivity.class);
                startActivityForResult(intent, 0X0100);
            }
        });

        applyButton.setOnClickListener(v -> {
            Intent intent = new Intent(this,ApplyForm.class);
            startActivity(intent);
        });

        btMap.setOnClickListener(v -> {
            Intent intent = new Intent(this,ApplyForm.class);
            startActivity(intent);
        });
    }

    private void init() {
        mTaskList = new ArrayList<>();
        databaseHelper = new DatabaseHelper(this);
        taskAdapterView = new TaskAdapterView(mTaskList, this);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
    }

    private void getView() {
        rvTask = findViewById(R.id.rv_task_list);
        addView = findViewById(R.id.add_task);
        applyButton = findViewById(R.id.bt_apply_form);
        btMap = findViewById(R.id.bt_map);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTaskList.clear();
        getAllTask();
    }

    public void getAllTask() {
        Cursor cursor = databaseHelper.fetchAllTask();
        Date date = new Date();
        date.getTime();
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setId(cursor.getInt(0));
                task.setTaskName(cursor.getString(1));
                task.setDate(cursor.getLong(2));
                System.out.println("task --> " + task);
                setNotificationAlarm(task);
                mTaskList.add(task);
            }
        }
        taskAdapterView.notifyDataSetChanged();
    }

    private void setNotificationAlarm(Task task) {
        long futureInMillis = task.getDate();
        System.out.println("currentTimeMis Time : " + System.currentTimeMillis());
        Intent notificationReceiverIntent = new Intent(this, NotificationReceiver.class);
        notificationReceiverIntent.putExtra(NotificationReceiver.NOTIFICATION_ID, task.getId());
        notificationReceiverIntent.putExtra(NotificationReceiver.NOTIFICATION, task.getTaskName());
        System.out.println(NotificationReceiver.NOTIFICATION + " : " + "hello world ");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), task.getId(), notificationReceiverIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        try {
            alarmManager.cancel(pendingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);
        }

        /* Driver driver = new GooglePlayDriver(this);
        FirebaseJobDispatcher firebaseJobDispatcher = new FirebaseJobDispatcher(driver);

        Job constraintReminderJob = firebaseJobDispatcher.newJobBuilder()
                .setService(TaskReminderService.class)
                .setTag(String.valueOf(task.getId()))
                .setConstraints(Constraint.DEVICE_CHARGING)
                .setLifetime(Lifetime.FOREVER)
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(Math.round(futureInMillis / 1000), Math.round(futureInMillis / 1000) + 60))
                .setReplaceCurrent(true)
                .build();

        firebaseJobDispatcher.schedule(constraintReminderJob);*/

        /*FirebaseMessaging instance = FirebaseMessaging.getInstance();
        instance.subscribeToTopic("task_notification").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                if (task.isSuccessful()) {
                    System.out.println("Fire_baseMessaging task complete");
                    Toast.makeText(getApplicationContext(), "Notification Send", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onTaskClick(Task task) {
        Intent intent = new Intent(this, AddTaskActivity.class);
        intent.putExtra(Common.EXTRA_TASK, task);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0X0100) {
            if(resultCode == RESULT_OK) {
                Task task = data.getParcelableExtra("Task");
            }
        }
    }

}
