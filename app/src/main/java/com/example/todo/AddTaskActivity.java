package com.example.todo;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;

import com.example.todo.database.DatabaseHelper;
import com.example.todo.modal.Task;

import java.util.Calendar;
import java.util.Locale;

public class AddTaskActivity extends AppCompatActivity {

    private int alarmYear;
    private int alarmMonth;
    private int alarmDay;
    private boolean isRequestUpadte;

    private String taskName;

    private EditText etTaskName;
    private TextView etDueDate;
    private TextView etDueTime;
    private Button btSetDueDate;
    private Button btSetDueTime;
    private DatabaseHelper databaseHelper;
    private Button btSubmit;
    private Task task;
    private Calendar setDateCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        getView();
        init();
        setListener();
        operation();
    }

    private void operation() {
        Intent intent = getIntent();
        task = intent.getParcelableExtra(Common.EXTRA_TASK);
        if (task != null) {
            isRequestUpadte = true;
            etTaskName.setText(task.getTaskName());
            setDateCalendar.setTimeInMillis(task.getDate());
            setDate(setDateCalendar.get(Calendar.DATE), setDateCalendar.get(Calendar.MONTH) + 1, setDateCalendar.get(Calendar.YEAR));
            etDueDate.setText(String.format(Locale.US, "%d/%d/%d", alarmDay, alarmMonth, alarmYear));
            etDueTime.setText(String.format(Locale.US, "%d:%2d", setDateCalendar.get(Calendar.HOUR), setDateCalendar.get(Calendar.MINUTE)));
            btSubmit.setText("Update Task");
        } else {
            task = new Task();
        }
    }

    private void setListener() {
        setDateCalendar = Calendar.getInstance();

        btSetDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                final int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddTaskActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                setDate(year, monthOfYear, dayOfMonth);
                                setDateCalendar.set(Calendar.DATE, dayOfMonth);
                                setDateCalendar.set(Calendar.MONTH, monthOfYear);
                                setDateCalendar.set(Calendar.YEAR, year);
                                etDueDate.setText(String.format(Locale.US, "%d/%d/%d", dayOfMonth, monthOfYear + 1, year));
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btSetDueTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int hour = cldr.get(Calendar.HOUR_OF_DAY);
                int minutes = cldr.get(Calendar.MINUTE);
                // time picker dialog
                TimePickerDialog picker = new TimePickerDialog(AddTaskActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
                                etDueTime.setText(sHour + ":" + sMinute);
                                setDateCalendar.set(Calendar.HOUR, sHour);
                                setDateCalendar.set(Calendar.MINUTE, sMinute);
                                System.out.println("set alarm time : " + setDateCalendar.getTimeInMillis());
                            }
                        }, hour, minutes, true);

                picker.show();
            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskName = String.valueOf(etTaskName.getText());
                task.setTaskName(taskName);
                task.setDate(setDateCalendar.getTimeInMillis());
                Intent intent = new Intent();
                intent.putExtra("Task", task);
                setResult(RESULT_OK, intent);
                finish();
                if (isRequestUpadte)
                    databaseHelper.update(taskName, setDateCalendar, task.getId());
                else
                    databaseHelper.insert(taskName, setDateCalendar, true);
            }
        });
    }

    private void setDate(int year, int month, int date) {
        alarmYear = year;
        alarmMonth = month;
        alarmDay = date;
    }

    private void init() {
        databaseHelper = new DatabaseHelper(this);
        isRequestUpadte = false;
    }

    private void getView() {
        btSubmit = findViewById(R.id.bt_add_task);
        etTaskName = findViewById(R.id.et_task_name);
        etDueDate = findViewById(R.id.tv_due_date);
        etDueTime = findViewById(R.id.tv_due_time);
        btSetDueDate = findViewById(R.id.bt_set_due_date);
        btSetDueTime = findViewById(R.id.bt_set_due_time);
    }
}
